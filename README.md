# Cooler Bag magnetic holder on a bike rack

![](pictures/DSC_6432.jpg)
![](pictures/DSC_6428.jpg)

**What is it?**

A holder for bicycle rack bag  with magnetic lock. Using plywood as a base for the bag and 3D printed holder with magnets attached to the bicycle.

**Why build it?**

After purchasing [a cooler bag for a bicycle](https://www.amazon.ca/gp/product/B093P82999/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1) it turns out it's not a very practical solution.

Cooler bag is wider than a rack so once attached with the straps it bends around the rack so you loose width and it becomes just as wide as a rack.

Also, if bag is sitting on top of the rack it blocks the access to hand side bags.

**Requirements**

* Provide a hard base for the bag so all width can be used.
* Lift the bag above the rack so there is space to hook side bags.
* Have a locking mechanism.

**BOM**

![](pictures/DSC_6384.jpg)

1x Holder (3D printed)
1x Bag base (plywood)
120x Magnets (round 8mm diameter x 3mm thickness)
5x zip ties

some tape to put on the rack to protect from scratching

**Assembly**

Measure your Rack dimensiosn and adjust the 3D printed holder FreeCAD file so the spacing is correct and rack bar diameter is the same.
In this design all spacing between rack bars are 10cm and bar diamenter is 1cm.

* Cut plywood piece.
* Drill 8mm holes for magnets. (I 3D printed small stencil based on orginigal design to guide holes)
* Insert/glue magents into the wood piece. I added 2x magents per hole. Be aware of magents' polarity.
* Glue magnets and cover holes with wood putty.
* Multiple Layers of sanding, priming, sanding, painting. (after using it for a week I spray it with epoxy that protects from scratching)
* Glue magnets into 3D printed piece.
* Sand, prime, paint 3D printed piece.
* Attach cooler bag to the wooden base. Putting straps into the slots so bag won't slide.
* Insert zip ties into 3D printed piece.
![](pictures/DSC_6395.jpg)
![](pictures/DSC_6397.jpg)
* Use zip ties to attach holder to the rack.
![](pictures/DSC_6437.jpg)
![](pictures/DSC_6440.jpg)



**Notes**

Chose to go with the magnets after multiple tests, but when bag is full after going full speed there is a risk of it detaching so use it with the staps too.

For future it would be nice to design something that is fully 3D printed and no magnets needed. Some sort of latching lock mechanism.

Designed usign FreeCAD 0.19 and assembly done using assembly4 workbench.
